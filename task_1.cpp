#include <iostream>
#include <exception>
#include <functional>
#include <vector>

enum status_t
{
    FIND_SUCCESS,
    FIND_ERROR,
    INSERT_SUCCESS,
    INSERT_ERROR,
    REMOVE_SUCCESS,
    REMOVE_ERROR
};

template <typename T>

class Comparator
{
    public:
        virtual int compare(const T& object_1, const T& object_2) const = 0;
        virtual ~Comparator() = default;
};

template <typename T>

class Default_comparator: public Comparator<T>
{
    int compare(const T& object_1, const T& object_2) const
    {
        if (object_1 > object_2)
        {
            return 1;
        }
        if (object_1 < object_2)
        {
            return -1;
        }
        return 0;
    }   
};

template <typename TKey, typename TValue>

struct Node
{
    TKey key;
    TValue value;
    Node(TKey k, TValue v)
    {
        key = k;
        value = v;
    }
    Node *left = nullptr;
    Node *right = nullptr;
};


template <typename TKey, typename TValue>

class Container
{ 
    public:
        virtual void insert(const Node<TKey, TValue>& object) = 0;
        virtual TValue find(const TKey& key) = 0; 
        virtual TValue remove(const TKey& key) = 0;
        virtual ~Container() = default;
};


template <typename TKey, typename TValue>

class BinaryTree: public Container<TKey, TValue>
{
    public:

        class TreeException: public std::exception
        {
            public:
            virtual const char* what() const noexcept = 0;
        };
        class ItemExist: public TreeException
        {
            public:
            TKey key;
            ItemExist(TKey _key)
            {
                key = _key;
            }
            const char* what() const noexcept 
            { 
                std::cout << "Key: " << key ;
                return "Item already has existed\n";                
            }
        };
        class ItemNoExist: public TreeException
        {
            public:
            TKey key;
            ItemNoExist(TKey _key): key { _key }
            {

            }
            const char* what() const noexcept 
            { 
                std::cout << "Key: " << key;
                return "Item is not exists\n";
            }
        };
        class InvalidKey: public TreeException
        {
            public:
            TKey key;
            InvalidKey(TKey _key): key { _key }
            {

            }
            const char* what() const noexcept 
            { 
                std::cout << "Key: " << key;
                return "Invalid key\n";
            }
        };

    protected:
        class InsertTemplate
        {
            public:
                status_t invokeInsert(Node<TKey, TValue> *& root, 
                                        TKey key, TValue value, 
                                        Comparator<TKey> *keyComparator);
            protected:
                virtual void insertHook(Node<TKey, TValue> *& root, 
                                        TKey key, TValue value, 
                                        Comparator<TKey> *keyComparator)
                {

                }
        };
        class FindTemplate
        {
            public:
                status_t invokeFind(Node<TKey, TValue> *& root, 
                                        TKey key, TValue &value, Comparator<TKey> *keyComparator);
            protected:
                virtual void findHook(Node<TKey, TValue> *& root, 
                                        TKey key, TValue value)
                {

                }
        };
        class RemoveTemplate
        {
            public:
                status_t invokeRemove(Node<TKey, TValue> *& root, 
                                        TKey key, TValue &value, 
                                        Comparator<TKey> *keyComparator);
            protected:
                virtual void removeHook(Node<TKey, TValue> *& root, 
                                        TKey key, TValue value, 
                                        Comparator<TKey> *keyComparator)
                {

                }
        };
    
    public:

        BinaryTree(InsertTemplate _inserter, FindTemplate _finder, 
                    RemoveTemplate _remover, Comparator<TKey> *compare = nullptr): 
                        inserter { _inserter }, finder { _finder },
                        remover { _remover }, comparator { compare }
        {

        } 
        BinaryTree(Comparator<TKey> *compare): 
                BinaryTree(InsertTemplate(), FindTemplate(), 
                            RemoveTemplate(), compare)
        {

        }
        BinaryTree(const BinaryTree<TKey, TValue>& object);
        ~BinaryTree();
        BinaryTree<TKey, TValue>& operator=(const BinaryTree<TKey, TValue>& object);


        typedef std::function<void(const TKey &k, const TValue &value, int depth)> callbackFunction;

        void insert(const Node<TKey, TValue>& object);
        TValue find(const TKey& key);
        TValue remove(const TKey& key);

        void prefix(callbackFunction) const;
        void postfix(callbackFunction) const;
        void infix(callbackFunction) const;

    
    private:
        Node<TKey, TValue> *root = nullptr; // корень дерева
        Comparator<TKey> *comparator = nullptr; // компаратор
        InsertTemplate inserter;
        FindTemplate finder;
        RemoveTemplate remover;
    private:
        void go_prefix(const Node<TKey, TValue>*, callbackFunction, int _depth) const;
        void go_postfix(const Node<TKey, TValue>*, callbackFunction, int _depth) const;
        void go_infix(const Node<TKey, TValue>*, callbackFunction, int _depth) const;
};

template <typename TKey, typename TValue>

void BinaryTree<TKey, TValue>::prefix(callbackFunction func) const
{
    go_prefix(root, func, 0);
}

template <typename TKey, typename TValue>

void BinaryTree<TKey, TValue>::go_prefix(const Node<TKey, TValue>* root, callbackFunction func, int _depth) const
{
    if (root != nullptr)
    {
        func(root->key, root->value, _depth);
        if (root->left)
        {
            go_prefix(root->left, func, _depth + 1);
        }
        if (root->right)
        {
            go_prefix(root->right, func, _depth + 1);
        }
    }
}

template <typename TKey, typename TValue>

void BinaryTree<TKey, TValue>::postfix(callbackFunction func) const
{
    go_postfix(root, func, 0);
}

template <typename TKey, typename TValue>

void BinaryTree<TKey, TValue>::go_postfix(const Node<TKey, TValue>* root, callbackFunction func, int _depth) const
{
    if (root != nullptr)
    {
        if (root->left)
        {
            go_prefix(root->left, func, _depth + 1);
        }
        if (root->right)
        {
            go_prefix(root->right, func, _depth + 1);
        }
        func(root->key, root->value, _depth);
    }
}

template <typename TKey, typename TValue>

void BinaryTree<TKey, TValue>::infix(callbackFunction func) const
{
    go_infix(root, func, 0);
}

template <typename TKey, typename TValue>

void BinaryTree<TKey, TValue>::go_infix(const Node<TKey, TValue>* root, callbackFunction func, int _depth) const
{
    if (root != nullptr)
    {
        if (root->left)
        {
            go_prefix(root->left, func, _depth + 1);
        }
        func(root->key, root->value, _depth);
        if (root->right)
        {
            go_prefix(root->right, func, _depth + 1);
        }
    }
}

template <typename TKey, typename TValue>

BinaryTree<TKey, TValue>::BinaryTree(const BinaryTree<TKey, TValue>& object):
                inserter{ object.inserter }, finder { object.finder },
                remover { object.remover}, comparator { object.comparator}
{
    std::vector<Node<TKey, TValue>> newTree;
    prefix([&newTree](TKey key, TValue value, int depth) { newTree.insert_back({key, value}); } );
    for (int i = 0; i < newTree.size(); i++)
    {
        this->insert(newTree[i]);
    }
}

template <typename TKey, typename TValue>

BinaryTree<TKey, TValue>& BinaryTree<TKey, TValue>::operator=(const BinaryTree<TKey, TValue>& object)
{
    this->inserter = object.inserter;
    this->finder = object.finder;
    this->remover = object.remover;
    this->comparator = object.comparator;
    std::vector<Node<TKey, TValue>> newTree;
    prefix([&newTree](TKey key, TValue value, int depth) { newTree.insert_back({key, value}); } );
    for (int i = 0; i < newTree.size(); i++)
    {
        this->insert(newTree[i]);
    }
    return *this;    
}

template <typename TKey, typename TValue>

status_t BinaryTree<TKey, TValue>::FindTemplate::invokeFind(Node<TKey, TValue> *& root, 
                                        TKey key, TValue &value, Comparator<TKey> *keyComparator)
{
    Node<TKey, TValue> *current = root;
    while (current)
    {
        switch (keyComparator->compare(key, current->key))
        {
        case -1:
            current = current->left;
            break;
        case 1:
            current = current->right;
            break;
        case 0:
            value = current->value;
            return FIND_SUCCESS;
        default:
            break;
        }
    }
    return FIND_ERROR;    
}

template <typename TKey, typename TValue>

status_t BinaryTree<TKey, TValue>::InsertTemplate::invokeInsert(Node<TKey, TValue> *&root, 
                                        TKey key, TValue value, Comparator<TKey> *keyComparator)
{
    Node<TKey, TValue> *insertNode = new Node<TKey, TValue>(key, value);
    if (!root)
    {
        root = insertNode;
        return INSERT_SUCCESS;
    }
    else
    {
        Node<TKey, TValue> *current = root;
        int last = 0;
        while (current)
        {
            last = keyComparator->compare(key, current->key);
            if (last == -1)
            {
                if (current->left)
                {
                    current = current->left;
                    continue;
                }
                break;
            }
            else if (last == 1)
            {
                if (current->right)
                {
                    current = current->right;
                    continue;
                }
                break;
            }
            else if (last == 0)
            {
                delete insertNode;
                return INSERT_ERROR;
            }
        }
        if (last == -1)
        {
            current->left = insertNode;
        }
        else if (last == 1)
        {
            current->right = insertNode;
        }
    }
    return INSERT_SUCCESS;    
}

template <typename TKey, typename TValue>

void BinaryTree<TKey, TValue>::insert(const Node<TKey, TValue>& object)
{
    status_t status = inserter.invokeInsert(this->root, object.key, object.value, this->comparator);
    if (status == INSERT_ERROR)
    {
        throw ItemExist(object.key);
    }
}

template <typename TKey, typename TValue>

TValue BinaryTree<TKey, TValue>::find(const TKey& key)
{
    TValue value;
    status_t status = finder.invokeFind(this->root, key, value, this->comparator);
    if (status == FIND_ERROR)
    {
        throw InvalidKey(key);
    }
    return value;
}

template <typename TKey, typename TValue>

status_t BinaryTree<TKey, TValue>::RemoveTemplate::invokeRemove(Node<TKey, TValue> *& root, 
                                        TKey key, TValue &value, Comparator<TKey> *keyComparator)
{
    if (!root)
    {
        return REMOVE_ERROR;
    }
    else if (!root->left && !root->right)
    {
        value = root->value;
        delete root;
        root = nullptr;
    }
    else
    {
        Node<TKey, TValue> *parent = root;
        Node<TKey, TValue> *current = root;
        while (int comparer = keyComparator->compare(key, current->key))
        {
            if (comparer == -1)
            {
                if (current->left)
                {
                    parent = current;
                    current = current->left;
                }
                else
                {
                    return REMOVE_ERROR;
                }

            }
            else if (comparer == 1)
            {
                if (current->right)
                {
                    parent = current;
                    current = current->right;
                }
                else
                {
                    return REMOVE_ERROR;
                }
            }
        }
        while(true)
        {
            if (current->left && current->right)
            {
                Node<TKey, TValue> *replaceNode = nullptr;
                Node<TKey, TValue> *replaceParentNode = nullptr;
                replaceNode = current->right;
                while (replaceNode)
                {
                    if (replaceNode->left)
                    {
                        replaceParentNode = replaceNode;
                        replaceNode = replaceNode->left;
                    }
                    else
                        break;
                }
                current->key = replaceNode->key;
                current->value = replaceNode->value;
                if (replaceNode->right)
                {
                    replaceParentNode->left = replaceNode->right;
                }
                else
                {
                    replaceParentNode->left = nullptr;
                }
                value = replaceNode->value;
                delete replaceNode;
                replaceNode = nullptr;
                break;
            }
            else if (current->left)
            {
                if (parent->left == current)
                    parent->left = current->left;
                else
                    parent->right = current->left;
                value = current->value;
                delete current;
                current = nullptr;
                break;
            }
            else if (current->right)
            {
                if (parent->left == current)
                    parent->left = current->right;
                else
                    parent->right = current->right;
                parent->right = current->right;
                value = current->value;
                delete current;
                current = nullptr;
                break;
            }
            else 
            {
                if (parent->left == current)
                {
                    parent->left = nullptr;
                }
                else
                {
                    parent->right = nullptr;
                }
                value = current->value;
                delete current;
                current = nullptr;
                break;
            }
        }      
    }
    return REMOVE_SUCCESS;
}

template <typename TKey, typename TValue>

TValue BinaryTree<TKey, TValue>::remove(const TKey& key)
{
    TValue value;
    status_t status = remover.invokeRemove(this->root, key, value, this->comparator);
    if (status == REMOVE_ERROR)
    {
        throw ItemNoExist(key);
    }
    return value;
} 

template <typename TKey, typename TValue>

BinaryTree<TKey, TValue>::~BinaryTree()
{
    std::vector<Node<TKey, TValue>> deleteTree;
    this->postfix([&deleteTree](TKey key, TValue value, int depth) { deleteTree.push_back({key, value}); });
    for(int i = 0; i < deleteTree.size(); i++)
    {
        remove(deleteTree[i].key);
    }
}

template <typename Key, typename Value>

void print_result(const Key &key, const Value &value, int depth)
{
    std::cout << "Key = " << key << "; " << "Value = " << value << "; " << "Depth = "<< depth << ";" <<std::endl;
}

int main()
{
    Comparator<int> *cmp = new Default_comparator<int>();
    BinaryTree<int, std::string> my_tree(cmp);
    int a = 1, b = 1, c = 3, d = 2, e = 0, f = 6;
    Node<int, std::string> node1(a, "11");
    Node<int, std::string> node2(c, "33");
    Node<int, std::string> node4(d, "22");
    Node<int, std::string> node5(e, "00");
    try
    {
        my_tree.insert(node1);
    }
    catch(BinaryTree<int,int>::ItemExist error)
    {
        std::cerr << error.what();
    }
    my_tree.prefix(print_result<int, std::string>);
    std::cout << "End\n";
    my_tree.insert(node2);
    my_tree.insert(node4);
    my_tree.insert(node5);
    my_tree.prefix(print_result<int, std::string>);
    try
    {
        my_tree.find(3);
    }
    catch(BinaryTree<int,std::string>::InvalidKey error)
    {
        std::cerr << error.what();
    }
    std::cout << "End\n";

    my_tree.remove(1);
    my_tree.prefix(print_result<int, std::string>);
    std::cout << "End\n";
}
